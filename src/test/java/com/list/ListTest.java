package com.list;

import org.junit.Test;

import static org.junit.Assert.*;

public class ListTest {

    @Test
    public void listShouldContainTheElementsAdded() {
        List list = new List();
        list.addFirst(1);
        list.addFirst(2);
        list.addLast(4);
        list.addFirst(7);
        list.addLast(9);
        assertEquals(list.toString(), "[7,2,1,4,9]");
    }

    @Test
    public void listShouldRemoveElements(){
        List list = new List();
        list.addFirst(2);
        list.addLast(4);
        list.addFirst(3);
        list.removeFirst();
        assertEquals(list.toString(), "[2,4]");
    }
}
