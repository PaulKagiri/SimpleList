package com.list;

class ListNode {

    int data;
    ListNode next;

    ListNode(int data) {
        this.data = data;
    }

    public int getData() {
        return data;
    }

    public ListNode getNext() {
        return next;
    }

    @Override
    public String toString() {
        return String.valueOf(data);
    }
}

public class List {

    private ListNode head;

    public void addFirst(int data) {
        ListNode newNode = new ListNode(data);
        //could be empty
        if (isEmpty()) {
            head = newNode;
        } else {
            ListNode current = head;
            head = newNode;
            head.next = current;
        }
    }

    public void addLast(int data) {
        ListNode newNode = new ListNode(data);
        //could be empty
        if (isEmpty()) {
            head = newNode;
        } else {
            ListNode current = head;
            //find the last node
            while (current.next != null) {
                current = current.next;
            }
            current.next = newNode;
        }
    }

    public int removeFirst() {
        if (isEmpty()) {
            throw new IllegalStateException("Empty List!");
        }

        int data = head.data;
        head = head.next;
        return data;
    }

    public int removeLast() {
        if (isEmpty()) {
            throw new IllegalStateException("Empty List!");
        }

        if (head.next == null) {
            int data = head.data;
            head = null;
            return data;
        }

        ListNode current = head;
        //find the last node
        while (current.next.next != null) {
            current = current.next;
        }
        int data = current.next.data;
        current.next = null;
        return data;
    }

    public boolean isEmpty() {
        return head == null;
    }

    public void display() {
        System.out.print(toString());
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("[");
        ListNode current = head;
        //find the last node
        while (current != null) {
            stringBuilder.append(current.data);
            stringBuilder.append(",");
            current = current.next;
        }
        if (stringBuilder.length() > 2) {
            stringBuilder = stringBuilder.deleteCharAt(stringBuilder.length() - 1);
        }
        stringBuilder.append("]");
        return stringBuilder.toString();
    }
}